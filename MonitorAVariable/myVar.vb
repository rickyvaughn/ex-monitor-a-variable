﻿Public Class myVar
    Private mValue As Integer
    Private mValue2 As Integer
    Public Event VariableChanged(ByVal mvalue As Integer)
    Public Event VariableChanged2(ByVal mvalue2 As Integer)
    Public Property Variable() As Integer
        Get
            Variable = mValue
        End Get
        Set(ByVal value As Integer)
            mValue = value
            RaiseEvent VariableChanged(mValue)
        End Set
    End Property

    Public Property Variable2() As Integer
        Get
            Variable2 = mValue2
        End Get
        Set(ByVal value2 As Integer)
            mValue2 = value2
            RaiseEvent VariableChanged2(mValue2)
        End Set
    End Property
End Class
